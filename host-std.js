const urlParser = require('url');
const bodyParser = require('body-parser');
const express = require('express');
const assert = require('assert');
const randomName = require('random-name');
const mongo = require('mongodb');
const dateFormat = require('dateformat');
const multer = require('multer');
const mongoClient = mongo.MongoClient;
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(__dirname + '/public')); // set static folder to serve images

var date = new Date();
console.log(dateFormat(date, "yyyymmddHHMMssl"));

// Connection URL
const url = 'mongodb://localhost:27017/sepm';
const port = 3002;

const collections = ["solved"]

// Create needed collections
mongoClient.connect(url, function(err, db){
    assert.equal(null, err);
    console.log("connected successfully to server");
    db.createCollection( collections[0], function() {
        if (err) throw err;
        console.log("collection created!");
        db.close();
    });
});

app.get('/solvedImages', function(req, res){
    const parameters = urlParser.parse(req.url, true).query;
    console.log("on /solvedImages PARAMETER: " + JSON.stringify(parameters));
    mongoClient.connect(url, function(err, db){
        assert.equal(null, err);
        db.collection(collections[0]).find().sort({_id:1}).limit(5).toArray(function(err, result){
            db.close();
            if (err) {
                console.log("something isn't right")
                return;
            };
            res.send(JSON.stringify(result));
        });
    });
});

app.post('/addSolved', function(req, res){
    console.log(req.body)
    const reqBody = req.body;
    console.log("on /addSolved PARAMETER: " + JSON.stringify(reqBody));
    const addingProduct = {
        name: reqBody.name,
        imageUrl: reqBody.imageUrl,
    };

    mongoClient.connect(url, function(err, db){
        assert.equal(null, err);
        db.collection(collections[0]).insertOne(addingProduct, function(err, res){
            db.close();
            if (err) {
                res.status(500).send("adding failed");
                console.log(err);
                return;
            };
        });
    });
    res.status(200).send("solved image added added");
});

// IMAGE UPLOAD
app.post('/image', function (req, res) {

    const storedFilename = dateFormat(date.now(), "yyyymmddHHMMssl") + '.jpg';
    const Storage = multer.diskStorage({
        destination: function (req, file, callback) {
            callback(null, __dirname + '/public/solvedImages');
        },
        filename: function (req, file, callback) {
            callback(null, storedFilename);
        }
    });
    const upload = multer({ storage: Storage }).single('solvedImage');
    
    upload(req, res, function (err) {
        if (err) {
            console.log(err);
            return res.status(500).send("Something went wrong !");
        }
        res.status(200).send("http://188.166.209.81:3002/solvedImages/" + storedFilename);
        return res.end();
    });
});

app.listen(port, function() {
    console.log("waiting at port " + String(port));
});
