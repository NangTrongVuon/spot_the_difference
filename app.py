#!/usr/bin/env python
#-*- coding: utf-8 -*-

from __future__ import print_function
import Pashua
import os.path
import image_difference
import requests
from io import BytesIO
from PIL import Image
import urllib

apiURL = "http://188.166.209.81:3002/"
global imagePath
global imageName
solving = False

# Set the images' paths relative to Pashua.app's path
app_bundle = os.path.dirname(os.path.dirname(Pashua.locate_pashua()))
icon = app_bundle + '/Resources/AppIcon@2.png'

def mainMenu():

	conf = u"""
	# Set window title
	*.title = Welcome to Spot the Difference

	# Introductory text
	txt.type = text
	txt.default = Spot the Difference is an application designed to highlight differences in two very similar image which has been laid side by side. [return][return] To quit the app, simply press Esc. To begin, select an option in the dialog below.\
	
	b1.type = button
	b1.label = Select image to process

	b2.type = button
	b2.label = View previously processed images

	db.type = defaultbutton
	db.label = Quit the application.
	"""

	if os.path.exists(icon):
	# Display Pashua's icon
		conf += u"""img.type = image
					img.x = 435
					img.y = 248
					img.maxwidth = 128
					img.tooltip = This is an element of type 'image'
					img.path = %s""" % icon

	result = Pashua.run(conf.encode('utf8'))

	# for key in result.keys():
	#     print("%s = %s" % (key, result[key]))
	#     if result[]
			
	#     if key == "txt":
	#         imageName = result[key]
	if result["b1"] == '1':
		print("running initial solve")
		solveMode()
	if result["b2"] == '1':
		print("fetching image")
		viewMode()

def solveMode():
	conf = u"""
	# Set window title
	*.title = Welcome to Spot the Difference

	# Introductory text
	txt.type = text
	txt.default = Spot the Difference works best if you give it an image that has clearly been separated by a whitespace or a line.\
	The image should be at a high enough resolution as low resolution images will result in bad results. [return][return]\
	To begin processing, select an image\
	in the dialog below. 

	# Add a text field
	tf.type = textfield
	tf.label = Image name
	tf.default = What is the name of your image?
	tf.width = 310
	tf.mandatory = true

	# Add a filesystem browser
	ob.type = openbrowser
	ob.label = Select an image 
	ob.width=310
	ob.filetype = jpg jpeg png
	ob.tooltip = Select the image to be processed here
	ob.mandatory = true

	# Add a cancel button with default label
	cb.type = cancelbutton
	cb.tooltip = Quit the app.

	db.type = defaultbutton
	db.tooltip = Sends the image to be processed.

	"""
	

	if os.path.exists(icon):
		# Display Pashua's icon
		conf += u"""img.type = image
					img.x = 435
					img.y = 248
					img.maxwidth = 128
					img.tooltip = This is an element of type 'image'
					img.path = %s""" % icon

	result = Pashua.run(conf.encode('utf8'))

	solving = False
	for key in result.keys():
			print("%s = %s" % (key, result[key]))
			if key == "ob":
				image = result[key]
				solving = True

			if key == "tf":
				name = result[key]
				
	
	if solving:
		image = image_difference.solveImage(image)
		binaryImage = open(image, 'rb')
		files = {'solvedImage': binaryImage}
		r1 = requests.post(apiURL + "image", files=files)
		imageLink = r1.text
		print(r1.text)

		newImage = {'name': name, 'imageUrl': imageLink}
		r2 = requests.post(apiURL + "addSolved", data=newImage)
		print(r2.text)
		solving = True
	else:
		print("back to main menu")
		mainMenu()

	while (solving):
		conf = u"""
		# Set window title
		*.title = Image processed

		# Introductory text
		txt.type = text
		txt.default = Your image has been processed successfully. If you want to process another image, simply repeat your process.[return][return]\

		img.type = image
		img.label = {imageName}
		img.x = 500
		img.y = 200
		img.maxwidth = 1024
		img.tooltip = The solved image.
		img.path = {imagePath}

		# Add a text field
		tf.type = textfield
		tf.label = Image name
		tf.default = What is the name of your image?
		tf.width = 310
		tf.mandatory = true

		# Add a filesystem browser
		ob.type = openbrowser
		ob.label = Select an image 
		ob.width=310
		ob.filetype = jpg jpeg png
		ob.tooltip = Select the image to be processed here
		ob.mandatory = true

		cb.type = cancelbutton
		cb.label = Go back to main menu
		
		db.type = defaultbutton
		db.tooltip = Process the image.

		""".format(imageName=name, imagePath=image)

		result = Pashua.run(conf.encode('utf8'))
		solving = False

		for key in result.keys():
				print("%s = %s" % (key, result[key]))
				if key == "ob":
					image = result[key]
					solving = True

				if key == "tf":
					name = result[key]   
		
		if solving:
			image = image_difference.solveImage(image)
			binaryImage = open(image, 'rb')
			files = {'solvedImage': binaryImage}
			r1 = requests.post(apiURL + "image", files=files)
			imageLink = r1.text
			print(r1.text)

			newImage = {'name': name, 'imageUrl': imageLink}
			r2 = requests.post(apiURL + "addSolved", data=newImage)
			print(r2.text)
			solving = True
		else:
			print("back to main menu")
			mainMenu()


def viewMode():
	conf = u"""
	# Set window title
	*.title = Welcome to Spot the Difference

	# Introductory text
	txt.type = text
	txt.default = The following are previously processed images uploaded to our databases.[return][return]\

	db.type = defaultbutton
	db.label = Back to Main Menu

	"""

	fetch = requests.get(apiURL + "solvedImages")
	
	fetchedData = fetch.json()

	count = 0
	for object in fetchedData:
		fetchedImageURL = object['imageUrl']
		fetchedImage = urllib.request.urlopen(fetchedImageURL).read()
		readImage = Image.open(BytesIO(fetchedImage))
		imageFileName = "img" + str(count) + ".jpg" 
		imagePashuaName = "img" + str(count)
		readImage.save(imageFileName)

		# print(imagePashuaName)
		imgX = count * 520
		imgY = 250
		
		conf += u"""{imagePashuaName}.type = image
					{imagePashuaName}.maxwidth = 512
					{imagePashuaName}.tooltip = This is a solved image.
					{imagePashuaName}.path = {imgPath}
					{imagePashuaName}.x = {imgX}
					{imagePashuaName}.y = {imgY}
					""".format(imagePashuaName=imagePashuaName, imgPath = imageFileName, imgX = imgX, imgY = imgY)
		count += 1

	# print(conf)
	result = Pashua.run(conf.encode('utf8'))

	mainMenu()

# Reading image from URL 
# Image.open(StringIO(urllib.urlopen(url).read()))
# r = requests.get(url = apiURL + "solvedImages")
# data = r.json()

# print(data)

mainMenu()
print("exiting app")