import cv2, sys, os
import numpy as np

from skimage.measure import compare_ssim

def blend_non_transparent(face_img, overlay_img):
    # Let's find a mask covering all the non-black (foreground) pixels
    # NB: We need to do this on grayscale version of the image
    gray_overlay = cv2.cvtColor(overlay_img, cv2.COLOR_BGR2GRAY)
    overlay_mask = cv2.threshold(gray_overlay, 1, 255, cv2.THRESH_BINARY)[1]

    # Let's shrink and blur it a little to make the transitions smoother...
    overlay_mask = cv2.erode(overlay_mask, cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3,3)))
    overlay_mask = cv2.blur(overlay_mask, (7,7))
    
    
    # And the inverse mask, that covers all the black (background) pixels
    background_mask = 255 - overlay_mask

    # Turn the masks into three channel, so we can use them as weights
    overlay_mask = cv2.cvtColor(overlay_mask, cv2.COLOR_GRAY2BGR)
    background_mask = cv2.cvtColor(background_mask, cv2.COLOR_GRAY2BGR)

    # Create a masked out face image, and masked out overlay
    # We convert the images to floating point in range 0.0 - 1.0
    face_part = (face_img * (1 / 255.0)) * (background_mask * (1 / 255.0))
    overlay_part = (overlay_img * (1 / 255.0)) * (overlay_mask * (1 / 255.0))

    # And finally just add them together, and rescale it back to an 8bit integer image
    return np.uint8(cv2.addWeighted(face_part, 255.0, overlay_part, 255.0, 0.0))

def processBounds(processedXBounds):
    """
    Process bounds to find the right pixels to crop an image at.

    """
    width1 = processedXBounds[0]
    width2 = width - processedXBounds[1]
    difference = width1 - width2

    print(difference)

    # If the difference is positive, this means left is bigger than right
    if difference < 0:
        processedXBounds[0] -= difference
    # Difference is negative, means right is bigger than left
    else:
        processedXBounds[1] += difference            

    print(processedXBounds)

def autocrop(image, threshold=0):
    """
    Crops any edges below or equal to threshold
    Crops blank image to 1x1.
    Returns cropped image.
    """
    if len(image.shape) == 3:
        flatImage = np.max(image, 2)
    else:
        flatImage = image
    assert len(flatImage.shape) == 2

    rows = np.where(np.max(flatImage, 0) > threshold)[0]
    if rows.size:
        cols = np.where(np.max(flatImage, 1) > threshold)[0]
        image = image[cols[0]: cols[-1] + 1, rows[0]: rows[-1] + 1]
    else:
        image = image[:1, :1]

    return image

def splitImage(image, point1, point2):
    """
    Splits image using two points - point 1 and point 2
    """
    height, width = image.shape[:2]
    start_row, start_col = int(0), int(0)
    end_row, end_col = int(height), int(point1) 
    cropped_left = image[start_row:end_row, start_col:end_col]

    start_row_right, start_col_right = int(0), int(point2)
    end_row_right, end_col_right = int(height), int(width)
    cropped_right = image[start_row_right:end_row_right, start_col_right:end_col_right]

    return cropped_left, cropped_right
    
def solveImage(filename):
    image_split = False
    # filename = sys.argv[1]
    image = cv2.imread(filename)

    # crop all borders first
    image = autocrop(image, 50)
    # image = autocrop(image, 200)
    height, width = image.shape[:2]

    # Not even
    if width % 2 != 0:
         image = cv2.copyMakeBorder(image,0,0,0,1,cv2.BORDER_REPLICATE)
         height, width = image.shape[:2]

    # Middle point, used to calculate correct contours
    middle_point = width / 2
    print("middle point: ", middle_point)

    gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
    _,thresh = cv2.threshold(gray,225,255,cv2.THRESH_BINARY)

    (_, contours, _) = cv2.findContours(thresh, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

    # for contour in contours:
    #     cv2.drawContours(image, contours, -1, (0, 0, 255), 0)

    # try splitting by the middle first
    if (width % 2 > 0):
        width = width - 1

    cropped_left, cropped_right = splitImage(image, width / 2, width / 2)
    cropped_left_size = cropped_left.shape[:2]
    cropped_right_size = cropped_right.shape[:2]

    print(cropped_left_size, cropped_right_size)

    if (cropped_left_size == cropped_right_size):
        print("equal!")
        image_split = True

    # test SSIM now
    grayLeft = cv2.cvtColor(cropped_left, cv2.COLOR_BGR2GRAY)
    grayRight = cv2.cvtColor(cropped_right, cv2.COLOR_BGR2GRAY)

    (score, diff) = compare_ssim(grayRight, grayLeft, full=True)

    diff = (diff * 255).astype("uint8")
    print("SSIM: {}".format(score))

    # if the SSIM is too low, meaning the image was split badly
    if score < 0.8:
        image_split = False

    # if contours are not equal
    if (not image_split):

        # try to adjust for differences
        xBounds = []
        processedXBounds = []

        for cnt in contours:
            
            # Extremely dirty hack
            # WIP: FIX THIS
            y = cnt[ : , : , 0:]

            for element in y:
                for specificElement in element:
                    if specificElement[1] == 0:
                        xBounds.append(specificElement[0])
        
        print("xbounds: ", xBounds)

        for value in xBounds:
            difference = abs(value - middle_point)
            print(value, difference)
            # not too far from center
            if difference <= 10:
                processedXBounds.append(value)

        print(processedXBounds)
        processedXBounds.sort()
        xBounds = processedXBounds[:]
        processedXBounds[1] = width - processedXBounds[0]
        print("after sort bounds", processedXBounds)
        # processBounds(processedXBounds)

        cropped_left, cropped_right = splitImage(image, processedXBounds[0], processedXBounds[1])

    grayLeft = cv2.cvtColor(cropped_left, cv2.COLOR_BGR2GRAY)
    grayRight = cv2.cvtColor(cropped_right, cv2.COLOR_BGR2GRAY)

    print(grayLeft.shape[:2])
    print(grayRight.shape[:2])

    (score, diff) = compare_ssim(grayRight, grayLeft, full=True)

    diff = (diff * 255).astype("uint8")
    print("SSIM: {}".format(score))

    if score < 0.8: 
        print("new x bounds:", xBounds)
        # If the image is separated by a line
        if (abs(xBounds[0] - xBounds[1])) <= 2:
            # Processes a 3rd time
            rightSide = width - xBounds[1]
            padDifference = xBounds[1] - rightSide + 1
            image = cv2.copyMakeBorder(image,0,0,0,padDifference,cv2.BORDER_REPLICATE)
            height, width = image.shape[:2]
            cropped_left, cropped_right = splitImage(image, xBounds[1], xBounds[1] + 1)
            grayLeft = cv2.cvtColor(cropped_left, cv2.COLOR_BGR2GRAY)  
            grayRight = cv2.cvtColor(cropped_right, cv2.COLOR_BGR2GRAY) 
            (score, diff) = compare_ssim(grayRight, grayLeft, full=True)
            diff = (diff * 255).astype("uint8")

    _, mask = cv2.threshold(diff, 75, 250, cv2.THRESH_BINARY_INV)

    # loop over the contours
    # compute the bounding box of the contour and then draw the
    # bounding box on both input images to represent where the two	# images differ
    (_, contours, _) = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    for contour in contours:
        contourArea = cv2.contourArea(contour)
        #Get the ratio of width and height
        (x,y,w,h) = cv2.boundingRect(contour)
        h_w_ratio = h/w
        # w_h_ratio = w/h

        if (1.4 < h_w_ratio <3 and len(contour)<75 and len(contour)>25 and contourArea > 62 and score > 0.9 ) :
            (x, y, w, h) = cv2.boundingRect(contour)
            # cv2.drawContours(image, contours, -1, (0, 0, 255), 1)
            cv2.rectangle(cropped_left, (x, y), (x + w, y + h), (255, 0, 255), 2)
            cv2.rectangle(cropped_right, (x, y), (x + w, y + h), (255, 0, 255), 2)

        if (contourArea > 12 and contourArea < 150 and 15 < len(contour)<50 and h_w_ratio < 1.3 and score > 0.9 ) :
            # (x, y, w, h) = cv2.boundingRect(contour)
            # cv2.drawContours(image, contours, -1, (0, 0, 255), 1)
            cv2.rectangle(cropped_left, (x, y), (x + w, y + h), (255, 0, 255), 1)
            cv2.rectangle(cropped_right, (x, y), (x + w, y + h), (255, 0, 255), 1)
        
        if (contourArea > 12 and contourArea < 150 and 15 < len(contour)<50 and h_w_ratio < 0.9 and score < 0.84) :
            # (x, y, w, h) = cv2.boundingRect(contour)
            # cv2.drawContours(image, contours, -1, (0, 0, 255), 1)
            cv2.rectangle(cropped_left, (x, y), (x + w, y + h), (255, 0, 255), 1)
            cv2.rectangle(cropped_right, (x, y), (x + w, y + h), (255, 0, 255), 1) 

        if (contourArea > 200 and len(contour)<20 and h_w_ratio < 3 ) :
            # (x, y, w, h) = cv2.boundingRect(contour)
            # cv2.drawContours(image, contours, -1, (0, 0, 255), 1)
            cv2.rectangle(cropped_left, (x, y), (x + w, y + h), (255, 0, 255), 2)
            cv2.rectangle(cropped_right, (x, y), (x + w, y + h), (255, 0, 255), 2)

        if (contourArea > 150 and h_w_ratio < 1.5) :
            # (x, y, w, h) = cv2.boundingRect(contour)
            # cv2.drawContours(image, contours, -1, (0, 0, 255), 1)
            cv2.rectangle(cropped_left, (x, y), (x + w, y + h), (255, 0, 255), 2)
            cv2.rectangle(cropped_right, (x, y), (x + w, y + h), (255, 0, 255), 2)

        if (contourArea > 120 and 2.5 > h_w_ratio > 2  ) :
            # (x, y, w, h) = cv2.boundingRect(contour)
            # cv2.drawContours(image, contours, -1, (0, 0, 255), 1)
            cv2.rectangle(cropped_left, (x, y), (x + w, y + h), (255, 0, 255), 2)
            cv2.rectangle(cropped_right, (x, y), (x + w, y + h), (255, 0, 255), 2)

        if (400 < contourArea and h_w_ratio < 2 and len(contour > 500)) :
            # (x, y, w, h) = cv2.boundingRect(contour)
            # cv2.drawContours(image, contours, -1, (0, 0, 255), 1)
            cv2.rectangle(cropped_left, (x, y), (x + w, y + h), (255, 0, 255), 2)
            cv2.rectangle(cropped_right, (x, y), (x + w, y + h), (255, 0, 255), 2)

        if (contourArea > 10 and score > 0.93 and h_w_ratio < 2) :
            # (x, y, w, h) = cv2.boundingRect(contour)
            # cv2.drawContours(image, contours, -1, (0, 0, 255), 1)
            cv2.rectangle(cropped_left, (x, y), (x + w, y + h), (255, 0, 255), 2)
            cv2.rectangle(cropped_right, (x, y), (x + w, y + h), (255, 0, 255), 2)

    cv2.imwrite("result.jpg", image)
    return os.path.dirname(os.path.abspath(__file__)) + "/result.jpg"