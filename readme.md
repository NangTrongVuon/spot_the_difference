Spot the Difference is an application designed to highlight differences in two very similar image which has been laid side by side. 

Requires Pashua, which can be downloaded here:
https://www.bluem.net/en/projects/pashua/

To run the app, download this repository and run the following command to download all of its dependencies:
**pip install -r requirements.txt**

Credits:

OpenCV
pyimagesearch.com
Pashua Framework
Hai Nguyen 
